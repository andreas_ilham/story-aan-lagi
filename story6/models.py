from django.db import models

# Create your models here.
class Kegiatan(models.Model):
    nama = models.CharField(max_length=64)
    def __str__(self):
        return self.nama

class Pesertanya(models.Model):
    nama = models.CharField(max_length=64)
    kegiatan = models.ForeignKey(Kegiatan, on_delete=models.DO_NOTHING)
    def __str__(self):
        return self.nama
