from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'andreaan.html')

def profile(request):
    return render(request, 'aan2.html')

def coming(request):
    return render(request, 'comingsoon.html')

