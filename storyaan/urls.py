from django.urls import path
from . import views

app_name = 'storyaan'

urlpatterns = [
    path('', views.home, name='andreaan'),
    path('data/', views.profile, name='aan2'),
    path('comingsoon/', views.coming, name='comingsoon'),
]