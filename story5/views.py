from django.shortcuts import render, get_object_or_404, redirect
from .models import MataKuliah
from .forms import *
from django.contrib import messages
from django.contrib.messages import constants

def index(request):
    all_matkul = MataKuliah.objects.all()
    return render(request, 'matakuliah.html', {'all_matkul': all_matkul})

def detail(request, matkul_id):
    matkul = get_object_or_404(MataKuliah, pk=matkul_id)
    return render(request, 'detail.html', {'matkul': matkul})

def add(request):
    if request.method == "POST":
        form = MataKuliahForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, (f"Mata kuliah {request.POST['nama_matkul']} berhasil ditambahkan!"))
            redirect('/')
        else:
            messages.warning(request, ("Data yang anda masukkan tidak sesuai kriteria."))
            redirect('/')
    return render(request, 'add.html')

def delete(request, matkul_id):
    matkul = MataKuliah.objects.get(id=matkul_id)
    matkul.delete()
    messages.success(request, (f"Mata Kuliah {matkul} berhasil dihapus"))
    return redirect('story5:index')

